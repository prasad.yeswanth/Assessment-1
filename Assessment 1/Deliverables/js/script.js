$(function() {


        var $regexname = /^[a-zA-Z\s]*$/;
        var $re = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        var $pass = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;


    $('#login').click(function() {

        if (localStorage.getItem("userDetails") != null) {

            var rujson = JSON.parse(localStorage.getItem("userDetails"));

            if (($('#inputEmail').val() != null) && ($('#inputPassword').val() != null)) {
                for (var i = 0; i < rujson.userInfo.length; i++) {
                    if ((rujson.userInfo[i].user.privilege.username == $('#inputEmail').val()) && (rujson.userInfo[i].user.privilege.password == $('#inputPassword').val())) {
                        if ((rujson.userInfo[i].user.privilege.is_admin) && (rujson.userInfo[i].user.privilege.username == "manager@admin.com")) {
                            $.session.set("user", "manager");
                             window.location.replace("admin.html");
                            return;
                        } else if(rujson.userInfo[i].user.privilege.is_admin){
                              $.session.set("user","admin");
                            window.location.replace("admin.html");
                            return;
                        }else {
                              $.session.set("user", "user");
                            window.location.replace("user.html");
                            return;
                        }

                    }
                }

                var msg="Please give valid credential";
                valid(msg);
                        
            } else {

                var msg="Fields should not be empty";
                valid(msg);
            }
        } else {
            var userDetails = {
                "userInfo": [{
                        "user": {
                            "userid": 521,
                            "privilege": {
                                "is_admin": false,
                                "first_name": "Test . G",
                                "username": "Test@gmail.com",
                                "password": "test"
                            }
                        }
                    },

                    {
                        "user": {
                            "userid": 530,
                            "privilege": {
                                "is_admin": true,
                                "first_name": "Yeswanth . G",
                                "username": "Yeshu564@gmail.com",
                                "password": "Yeshu564"
                            }
                        }
                    },
                      {
                        "user": {
                            "userid": 555,
                            "privilege": {
                                "is_admin": true,
                                "first_name": "Yeswanth . G",
                                "username": "manager@admin.com",
                                "password": "manager"
                            }
                        }
                    }
                    , {
                        "user": {
                            "userid": 564,
                            "privilege": {
                                "is_admin": false,
                                "first_name": "Yeshu",
                                "username": "Not_Admin@gmail.com",
                                "password": "Not_Admin"
                            }
                        }
                    }
                ]
            };

            // Put the object into storage
            localStorage.setItem("userDetails", JSON.stringify(userDetails));
        }

    });

function valid(msg){
    myvar = '<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
'  <div class="modal-dialog" role="document">'+
'    <div class="modal-content">'+
'      <div class="modal-header">'+
'        <h5 class="modal-title" id="exampleModalLabel">Error</h5>'+
'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
'          <span aria-hidden="true">×</span>'+
'        </button>'+
'      </div>'+
'      <div class="modal-body">'+
'        '+ msg+''+
'      </div>'+
'      <div class="modal-footer">'+
'        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
'      </div>'+
'    </div>'+
'  </div>'+
'</div>';

  $("#msg").append(myvar);

 $('#exampleModal').modal('show');

}  

});