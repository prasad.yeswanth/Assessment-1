$(function() {

    var arr = {};

    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("table tr").filter(function(index) {
            if (index > 0) {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            }
        });
    });

    if (localStorage.getItem("userExpe") != null) {
        var rujson = JSON.parse(localStorage.getItem("userExpe"));

        for (var i = 0; i < rujson.userExp.length; i++) {

            var boo = $.session.get("user");

            if ((boo == "admin" || boo == "manager")) {

                if( rujson.userExp[i].user.details.status =="Approved by manager"){
                     $("#dataTable").find('#addu')
                    .append($('<tr>')
                        .append($('<td>')
                            .text(rujson.userExp[i].user.userid))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.first_name))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.amt))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.doe))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.file))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.status))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.comments))
                        .append($('<td>')
                            .html('<button type="button" id=action disabled>' + "Action" + '</button>'))

                    );
                }else{
                     $("#dataTable").find('#addu')
                    .append($('<tr>')
                        .append($('<td>')
                            .text(rujson.userExp[i].user.userid))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.first_name))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.amt))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.doe))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.file))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.status))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.comments))
                        .append($('<td>')
                            .html('<button type="button" id=action>' + "Action" + '</button>'))
                        );  

                }

               
            } else {
                $("#dataTable").find('#addu')
                    .append($('<tr>')
                        .append($('<td>')
                            .text(rujson.userExp[i].user.userid))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.first_name))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.amt))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.doe))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.file))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.status))
                        .append($('<td>')
                            .text(rujson.userExp[i].user.details.comments))
                    );

            }

        }

        for (var i = rujson.userExp.length - 1; i < rujson.userExp.length; i++) {

            $("#dataTable").find('#addu1')
                .append($('<tr>')
                    .append($('<td>')
                        .text(rujson.userExp[i].user.userid))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.first_name))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.amt))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.doe))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.file))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.status))
                    .append($('<td>')
                        .text(rujson.userExp[i].user.details.comments))
                );
        }

    }

    $(document).on('click', '#addu  tr td button', function() {
        var boo = $.session.get("user");
        if (boo === "admin") {
            $(this).parents("tr").children().each(function() {
                arr[i] = this.innerText;
            });
            var rujson = JSON.parse(localStorage.getItem("userExpe"));
            // console.log(rujson.userExp[parseInt($(this).parents("tr").children().first().text())-1]["user"].details.status);
            rujson.userExp[parseInt($(this).parents("tr").children().first().text())]["user"].details.status = "Approved by admin"
            rujson.userExp[parseInt($(this).parents("tr").children().first().text())]["user"].details.comments = "Should be approved by manager"
            localStorage.userExpe = JSON.stringify(rujson);
            location.reload();
        } else if (boo === "manager") {
            $(this).parents("tr").children().each(function() {
                arr[i] = this.innerText;
            });
            var rujson = JSON.parse(localStorage.getItem("userExpe"));
            // console.log(rujson.userExp[parseInt($(this).parents("tr").children().first().text())-1]["user"].details.status);
            rujson.userExp[parseInt($(this).parents("tr").children().first().text())]["user"].details.status = "Approved by manager"
            rujson.userExp[parseInt($(this).parents("tr").children().first().text())]["user"].details.comments = "Will be added to your bank"

            localStorage.userExpe = JSON.stringify(rujson);
            location.reload();
        }

    });

    $('#uexpens').click(function() {

        if (localStorage.getItem("userExpe") != null) {

            var rujson = JSON.parse(localStorage.getItem("userExpe"));

            var name = $('#name').val().length;

            console.log(name);

            var currDate = new Date();
            var d = new Date($('#dop').val());
            var diff = (currDate.getTime() - d.getTime()) / 1000;
            diff /= (60 * 60 * 24);
            if (name >= 8) {
                if (($('#amt').val() >= 10) && ($('#amt').val() <= 99)) {
                    if (($('#dop').val().length > 0) && (diff >= 1 && diff <= 30)) {
                        var user = {
                            "userid": rujson.userExp.length,
                            "details": {
                                "is_approve": false,
                                "first_name": $('#name').val(),
                                "amt": $('#amt').val(),
                                "doe": $('#dop').val(),
                                "file": $('#file-upload').val(),
                                "status": "Not Approved",
                                "comments": ""
                            }

                        }

                        rujson.userExp.push({
                            user
                        });

                        localStorage.setItem("userExpe", JSON.stringify(rujson));
                        var msg = "Sucessfully added";
                        valid(msg);

                        location.reload();
                    } else {
                        var msg = "Please enter valid DOE (no currDate is allowed)";
                        valid(msg);

                    }
                } else {
                    var msg = "Please amt should be less than 100";
                    valid(msg);

                }
            } else {
                var msg = "Name should be atleast 8 char";
                valid(msg);

            }

        } else {
            var userExpe = {
                "userExp": [{
                        "user": {
                            "userid": 1,
                            "details": {
                                "is_approve": false,
                                "first_name": "Test . G",
                                "amt": "100",
                                "doe": "test",
                                "file": "ads",
                                "status": "Not Approved",
                                "comments": "test"
                            }
                        }
                    },

                    {
                        "user": {
                            "userid": 2,
                            "details": {
                                "is_approve": true,
                                "first_name": "Yeswanth . G",
                                "amt": "101",
                                "doe": "Yeshu564",
                                "file": "asd",
                                "status": "Not Approved",
                                "comments": "test"
                            }
                        }
                    }, {
                        "user": {
                            "userid": 3,
                            "details": {
                                "is_approve": false,
                                "first_name": "Yeshu",
                                "amt": "102",
                                "doe": "Not_Admin",
                                "file": "asdad",
                                "status": "Not Approved",
                                "comments": "test"
                            }
                        }
                    }
                ]
            };

            // Put the object into storage
            localStorage.setItem("userExpe", JSON.stringify(userExpe));
        }

        function valid(msg) {
            myvar = '<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                '  <div class="modal-dialog" role="document">' +
                '    <div class="modal-content">' +
                '      <div class="modal-header">' +
                '        <h5 class="modal-title" id="exampleModalLabel">Error</h5>' +
                '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                '          <span aria-hidden="true">×</span>' +
                '        </button>' +
                '      </div>' +
                '      <div class="modal-body">' +
                '        ' + msg + '' +
                '      </div>' +
                '      <div class="modal-footer">' +
                '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>';

            $("#msg").append(myvar);

            $('#exampleModal').modal('show');

        }

    });

});